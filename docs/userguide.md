# User guide

## Pull an image

    $ podman pull gitlab-registry.cern.ch/legac/p400-container/p400-dev

## Run an image in a podman container

 * Standalone:

        $ podman run -it --rm p400-dev bash
        (venv) [p400] $

 * Mounting your working directory:
    
        $ podman run -it --rm -v <absolute path working directory>:/p400 p400-dev bash
        (venv) [p400] $
    
* Detach mode (standalone)

        $ podman run -d p400-dev --name foo tail -f /dev/null
        $ podman ps -a
        $ podman exec -it foo bash
        (venv) [p400] $

        $ podman stop foo
        $ podman rm foo
