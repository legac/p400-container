--------------------------------- CHANGELOG ----------------------------------

1.0.0 (Mar 2024)
  - initial image to read and write I²C registers via the PCIe bus
