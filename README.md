# p400-container

## Features
* Projet to create docker images for the PCIe400 board.

## Documentation
* [User guide](docs/userguide.md)
* [Development guide](docs/devguide.md)

## Changelog
Consult the [Changelog](CHANGELOG.md) page for fixes and enhancements.

## Authors
* [List of Authors](AUTHORS)
* [List of Contributors](CONTRIBUTING)

## License
Copyright &#169; PCIe400 Team CNRS/IN2P3, 2024. \
Distributed under the terms of the [CeCILL](LICENSE) license

The packages `p400-container` is free and open source software.
